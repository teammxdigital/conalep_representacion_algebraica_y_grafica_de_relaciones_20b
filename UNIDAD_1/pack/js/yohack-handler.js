// Created By Camilo Medina // dev.cmedina@gmail.com 
// At some point on May 2015
// HANDLER FOR eBOOK <--> APP Interop

var yohack_handler = {};

yohack_handler.getEvaluationById = function(eID){
	return document.getElementById(eID).innerHTML;
}

yohack_handler.getEvaluationByIdDroid = function(eID){
    android.setNotaEvaluacion(document.getElementById(eID).innerHTML);
}

yohack_handler.getSelectedText = function(){
	var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}

